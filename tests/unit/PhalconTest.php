<?php

use Baka\Hmac\AuthMessage;
use Baka\Hmac\Encrypt;
use Baka\Hmac\Models\Keys;

class PhalconTest extends PhalconUnitTestCase
{
    /**
     * /
     * @return [type] [description]
     */
    public function testEncryption()
    {
        $di = $this->_getDI();

        $time = microtime(true);
        $hash = null;
        $data = [];
        $keys = Keys::findFirst();

        $authMessage = new AuthMessage($keys->public, $time, $hash, $data);
        $message = $authMessage->build();

        $hash = Encrypt::generate($message, $keys->private);

        //api call header
        $headers = ['APIPUB' => $keys->public, 'APITIME' => $time, 'APIHASH' => $hash];

        //verify Header to send
        $this->assertArrayHasKey('APIPUB', $headers);
        $this->assertArrayHasKey('APITIME', $headers);
        $this->assertArrayHasKey('APIHASH', $headers);

        $publicKey = $headers['APIPUB'];
        $time = $headers['APITIME'];
        $hash = $headers['APIHASH'];

        $clientMessage = new AuthMessage($publicKey, $time, $hash, $data);

        // build the server has
        $data = $clientMessage->build();
        $serverHash = Encrypt::generate($data, $keys->private);

        //get the client hash
        $clientHash = $clientMessage->getHash();

        //verify the sent has is the same as the read hash
        verify($hash)->equals($clientHash);
    }

    protected function setUp()
    {
    }

    protected function tearDown()
    {
    }

}
