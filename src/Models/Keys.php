<?php

namespace Baka\Hmac\Models;

class Keys extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $site;

    /**
     *
     * @var string
     */
    public $public;

    /**
     *
     * @var string
     */
    public $private;

    /**
     *
     * @var string
     */
    public $added_date;

    /**
     *
     * @var string
     */
    public $updated_date;

    /**
     *
     * @var integer
     */
    public $users_id;

    /**
     *
     * @var integer
     */
    public $company_id;

    public function getSource()
    {
        return 'api_keys';
    }

    /**
     * Generate the site public and private keys
     *
     * @return Array
     */
    public static function generate(string $siteName, int $userId, int $companyId)
    {
        $hash = hash('sha512', openssl_random_pseudo_bytes(64));

        // Base62 Encode the hash, resulting in an 86 or 85 character string
        $hash = gmp_strval(gmp_init($hash, 16), 62);

        // Chop and send the first 80 characters back to the client

        $public = substr($hash, 0, 32);
        $secret = substr($hash, 32, 48);

        $keys = new self();
        $keys->site = $siteName;
        $keys->public = $public;
        $keys->private = $secret;
        $keys->users_id = $userId;
        $keys->company_id = $companyId;
        $keys->added_date = date('Y-m-d H:i:s');
        $keys->updated_date = $keys->added_date;

        if (!$keys->save()) {
            throw new \Exception($keys->getMessages()[0]);
        }

    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap()
    {
        return array(
            'id' => 'id',
            'site' => 'site',
            'public' => 'public',
            'private' => 'private',
            'added_date' => 'added_date',
            'updated_date' => 'updated_date',
            'users_id' => 'users_id',
            'company_id' => 'company_id',
        );
    }

}
