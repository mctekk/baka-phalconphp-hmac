<?php

/**
 * Api HMAC Authentification
 *
 * @package Micro
 * @subpackage Messages
 */
namespace Baka\Hmac;

use Baka\Hmac\Encrypt as HmacEncrypt;
use Baka\Hmac\Models\Keys;
use Phalcon\Http\Request;
use Phalcon\DI;
use \Exception;

class Api
{
    protected $request;

    /**
     * Constructor
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Authenticate the request for ws methods
     *
     * @param  $getHttpMethods URI method type (GET | PUT | POST)
     * @return boolean
     */
    public function authenticate($getHttpMethods)
    {
        // Get Authentication Headers
        $publicKey = $this->request->getHeader('APIPUB');
        $time = $this->request->getHeader('APITIME');
        $hash = $this->request->getHeader('APIHASH');

        /**
         * por la shit de API v0 que hicimos para el 301 de la enciclopedia,
         * si el url que viene es para getNdoUrl no verificamos seguridad -_-
         *
         * @todo  remove
         * @deprecated
         */
        $bugFixed = explode('/', $this->request->get()['_url']);
        if (array_key_exists(2, $bugFixed) && $bugFixed[2] == 'getNdoUrl') {
            return true;
        }

        // does the public key send by the header exist?
        if ($apiKey = Keys::findFirstByPublic($publicKey)) {
            $data = [];
            $uploads = [];

            /**
             * if the user is trying to upload a image
             * @todo move this to a only function taht we call for this method and SdkTrait
             */
            if ($this->request->hasFiles()) {
                foreach ($this->request->getUploadedFiles() as $file) {
                    $uploads[] = [
                        'name' => $file->getKey(),
                        'filename' => $file->getName(),
                        'contents' => file_get_contents($file->getTempName()),
                    ];
                }

                $parseUpload = function ($request) use (&$uploads) {
                    foreach ($request as $key => $value) {
                        if (is_array($value)) {
                            foreach ($value as $f => $v) {
                                $uploads[] = ['name' => $key . '[' . $f . ']', 'contents' => $v];
                            }
                        } else {
                            $uploads[] = ['name' => $key, 'contents' => $value];
                        }
                    }
                };
            }

            //get the data sent by the API request
            switch ($getHttpMethods) {

                case 'GET':
                    $data = $this->request->getQuery();
                    unset($data['_url']);
                    break;

                case 'POST':
                    if (!$uploads) {
                        if (count($this->request->getPost()) > 0) {
                            $data = $this->request->getPost();
                        } elseif ($this->request->getJsonRawBody()) {
                            $data  = json_decode($this->request->getRawBody(), true);
                        }
                    } else {
                        $parseUpload($this->request->getPost());
                        $data = $uploads;
                    }
                    break;

                case 'PUT':
                    if (!$uploads) {
                        if (count($this->request->getPut()) > 0) {
                            $data =   $this->request->getPut();
                        } elseif ($this->request->getJsonRawBody()) {
                            $data =  json_decode($this->request->getRawBody(), true);
                        }
                    } else {
                        $parseUpload($this->request->getPut());
                        $data = $uploads;
                    }
                    break;

                default:
                    $data = $this->request->get();
                    if (array_key_exists('_url', $data)) {
                        unset($data['_url']);
                    }

                    break;
            }

            $cleanData = $data;

            $message = new AuthMessage($publicKey, $time, $hash, $data);

            // build the server has
            $data = $message->build();

            $serverHash = HmacEncrypt::generate($data, $apiKey->private);

            //get the client hash
            $clientHash = $message->getHash();
            //return "{$clientHash} === {$serverHash}";
            // ok so it matches ^^ we are almost good to go
            if ($clientHash === $serverHash) {
                $serverMicrotime = microtime(true);
                $timeDiff = $serverMicrotime - $time;

                /*
                 * Uses the header value timestamp to check against the current timestamp
                 * If the request was made within a reasonable amount of time (10 sec),
                 */
                if ($timeDiff <= 10) {
                    return true;
                } else {
                    throw new Exception("Request older then 10 seconds");
                }
            } else {
                DI::getDefault()->getLog()->error("Hashes mistmatch " . $getHttpMethods . " {$clientHash} === {$serverHash}", ['post' => $this->request->getPost(), 'put' => $this->request->getPut(), 'get' => $this->request->getQuery(), 'upload' => $uploads]);
                throw new Exception("Hashes mistmatch");
            }
        }
        DI::getDefault()->getLog()->error("Unauthenticated request", ['post' => $this->request->getPost(), 'put' => $this->request->getPut(), 'get' => $this->request->getQuery()]);
        throw new Exception("Unauthenticated request");
    }
}
