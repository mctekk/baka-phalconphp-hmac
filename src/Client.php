<?php

/**
 * Api HMAC Client Authentification
 *
 * @package Micro
 * @subpackage Messages
 */
namespace Baka\Hmac;

use Baka\Hmac\AuthMessage;
use Baka\Hmac\Encrypt;
use GuzzleHttp\Client as GuzzleClient;
use Phalcon\Mvc\Dispatcher;

/**
 * Call back function to connect with the API , think of it has our SDK -_-
 *
 */

class Client
{

    protected $client;
    public $url;
    protected $time;
    protected $apiKey;
    protected $publicKey;
    protected $privateKey;
    public $headers;
    protected $data;

    /**
     * Constructor inicialize Guzzle and api url
     * @return void
     */
    public function __construct($publicKey, $privateKey, array $data = [], array $headers = [])
    {
        $this->url = getenv('EXT_API_URL');
        $this->time = microtime(true);
        $this->publicKey = $publicKey;
        $this->privateKey = $privateKey;
        $this->headers = $headers;
        $this->data = $data; //initialize empty data
    }

    /**
     * /
     * @return [type] [description]
     */
    public function getGuzzle()
    {
        $this->hmacHeader();

        //set header
        return new GuzzleClient(['headers' => $this->headers]);
    }

    /**
     * Generate the API header for callback funtcions
     *
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return void
     */
    public function hmacHeader()
    {
        //generate the authentification message
        $hash = null;
        $authMessage = new AuthMessage($this->publicKey, $this->time, $hash, $this->data);
        $message = $authMessage->build();

        //create the hash base on the message
        $hash = Encrypt::generate($message, $this->privateKey);

        //api call header
        $this->headers = array_merge(
            $this->headers,
            ['APIPUB' => $this->publicKey, 'APITIME' => $this->time, 'APIHASH' => $hash]
        );
    }
}
